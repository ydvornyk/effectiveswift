//
//  OnboardingVM.swift
//  EffectiveSwift
//
//  Created by Yurii Dvornyk on 9/29/19.
//  Copyright © 2019 Yurii Dvornyk. All rights reserved.
//

import Foundation

import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit
import Bond

class OnboardingVM: VCVM {

    let firstName = Property("First Name")
    let lastName = Property("Last Name")
}
