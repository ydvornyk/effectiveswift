//
//  User.swift
//  EffectiveSwift
//
//  Created by Yurii Dvornyk on 9/29/19.
//  Copyright © 2019 Yurii Dvornyk. All rights reserved.
//

import Foundation

struct User {
    let firstName: String
    let lastName: String
    let email: String
    let password: String
}
