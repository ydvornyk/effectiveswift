//
//  AppContext.swift
//  EffectiveSwift
//
//  Created by Yurii Dvornyk on 9/29/19.
//  Copyright © 2019 Yurii Dvornyk. All rights reserved.
//

import Foundation
import ReaktiveBoar
import Swinject
import SwinjectAutoregistration
import ReactiveKit

extension UIApplication: Assembly {

    public func assemble(container: Container) {
        container.register(value: Property<User?>(nil)).inObjectScope(.container)
    }
}
