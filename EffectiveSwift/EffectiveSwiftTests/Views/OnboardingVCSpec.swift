//
//  OnboardingVCSpec.swift
//  EffectiveSwiftTests
//
//  Created by Yurii Dvornyk on 9/29/19.
//  Copyright © 2019 Yurii Dvornyk. All rights reserved.
//

import Foundation
import Quick
import UIKit
import Nimble
@testable import EffectiveSwift

class OnboardingVCSpec: QuickSpec {

    override func spec() {
        describe("Orboarding View Controller") {
            it("Shoulbe be created") {
                let vc = UIStoryboard(name: OnboardingVC.className,
                                      bundle: Bundle(for: OnboardingVC.self))
                    // swiftlint:disable:next force_cast
                    .instantiateInitialViewController() as! OnboardingVC
                vc.beginAppearanceTransition(true, animated: false)
                vc.endAppearanceTransition()
                expect(vc.firstNameTextField).notTo(beNil())
            }
        }
    }
}
